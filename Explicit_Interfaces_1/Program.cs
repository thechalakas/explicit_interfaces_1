﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Explicit_Interfaces_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Water_Bottle_1 temp_1 = new Water_Bottle_1();
            Water_Bottle_2 temp_2 = new Water_Bottle_2();

            //the following works
            temp_1.return_something();

            //the following wont
            //temp_2.return_something();

            //however, I can access return something through WaterBottle2 if I do this
            IWaterBottle temp_3 = new Water_Bottle_2();
            temp_3.return_something();

            //so what is happening is, Water_Bottle_2 is the class that is actually implementing
            //the interface
            //however, I dont want objects of type Water_Bottle_2 to access the interface methods

        }
    }

    //a simple Interface
    interface IWaterBottle
    {
        //has a method that simply returns something
        //since the type is object, I can return any type I want.
        object return_something();
    }

    //two implementations
    //this is implicit implementation
    class Water_Bottle_1 : IWaterBottle
    {
        //see how I am referencing the method itself with no link to the interface
        public object return_something()
        {
            return "hello there - implicit";
        }
    }

    //this is explicit implementation
    class Water_Bottle_2 : IWaterBottle
    {
        //see how I am referencing the interface and then its method
        //this is what makes it explicit
        object IWaterBottle.return_something()
        {
            return "hello there - explicit";
        }
    }
}
